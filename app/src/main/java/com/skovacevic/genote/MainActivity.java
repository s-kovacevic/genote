package com.skovacevic.genote;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {
    public String TAG = "MainActivity";

    private TextView tvCurrentNote;
    private EditText etSecondsToSleep;
    private Timer timer;
    private final Handler handler = new Handler();
    private ArrayList<String> notes = new ArrayList<>(Arrays.asList(
            "A", "A#", "Bb", "B", "C", "C#", "Db", "D", "D#", "Eb", "E", "F", "F#", "Gb", "G", "G#", "Ab"
    ));
    private ArrayList<String> variations = new ArrayList<>(Arrays.asList(
            "", "m"
    )); // Major or minor only for now

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupWidgets();
    }

    final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateCurrentNote();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        startTimer(Integer.parseInt(etSecondsToSleep.getText().toString()));
    }

    private String getRandomNote() {
        Random random = new Random();
        return notes.get(random.nextInt(notes.size())) +
                variations.get(random.nextInt(variations.size()));
    }

    private void updateCurrentNote() {
        tvCurrentNote.setText(getRandomNote());
    }

    private void setupWidgets() {
        tvCurrentNote = findViewById(R.id.currentNote);
        etSecondsToSleep = findViewById(R.id.etSecondsToSleep);

        etSecondsToSleep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    Integer secondsToSleep = Integer.parseInt(editable.toString());
                    timer.cancel();
                    startTimer(secondsToSleep);
                    Log.d(TAG, "afterTextChanged: " + secondsToSleep.toString());
                } catch (NumberFormatException e) {
                    Log.d(TAG, "afterTextChanged: Not a number son.");
                }

            }
        });
    }

    private void startTimer(Integer secondsToSleep) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 0, 1000 * secondsToSleep);
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }
}
